//
//  ViewController.h
//  Braintree
//
//  Created by User on 8/6/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Braintree/Braintree.h>

@interface ViewController : UIViewController <BTDropInViewControllerDelegate>

@property (nonatomic, strong) Braintree* braintree;

@end

