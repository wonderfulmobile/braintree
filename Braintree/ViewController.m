//
//  ViewController.m
//  Braintree
//
//  Created by User on 8/6/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import "ViewController.h"
#import <Braintree/Braintree.h>
#import <Braintree/BTClientCardRequest.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // TODO: Switch this URL to your own authenticated API
    NSURL *clientTokenURL = [NSURL URLWithString:@"http://playentertainmentnetwork.com/MOBILESTEVENGROUP.COM/server/braintree/pay_sandbox.php?client_token="];
    NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
    [clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection
     sendAsynchronousRequest:clientTokenRequest
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         // TODO: Handle errors in [(NSHTTPURLResponse *)response statusCode] and connectionError
         NSString *clientToken = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         // Initialize `Braintree` once per checkout session
         self.braintree = [Braintree braintreeWithClientToken:clientToken];
         
         // As an example, you may wish to present our Drop-In UI at this point.
         // Continue to the next section to learn more...
     }];
}

//NSString *strURL = [NSString stringWithFormat:@"http://www.api.postmobapp.com/PostMob/index.php?action=payment&client_token=%@", strContractorID];
//NSURL *clientTokenURL = [NSURL URLWithString:strURL];
//NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
//[clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
//
//[NSURLConnection
// sendAsynchronousRequest:clientTokenRequest
// queue:[NSOperationQueue mainQueue]
// completionHandler:^(NSURLResponse response, NSData data, NSError *connectionError) {
//     // TODO: Handle errors in [(NSHTTPURLResponse *)response statusCode] and connectionError
//     NSString *clientToken = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//     // Initialize `Braintree` once per checkout session
//     self.braintree = [Braintree braintreeWithClientToken:clientToken];
//     // Initialize `Braintree` once per checkout session
//     //self.braintree = [Braintree braintreeWithClientToken:clientToken];
//     
//     // As an example, you may wish to present our Drop-In UI at this point.
//     // Continue to the next section to learn more...
// }];

- (IBAction)tappedMyPayButton {
    
    
    [self paymentBrainTree:10];
    // If you haven't already, create and retain a `Braintree` instance with the client token.
    // Typically, you only need to do this once per session.
    //self.braintree = [Braintree braintreeWithClientToken:aClientToken];
    
    // Create a BTDropInViewController
//    BTDropInViewController *dropInViewController = [self.braintree dropInViewControllerWithDelegate:self];
//    // This is where you might want to customize your Drop in. (See below.)
//    
//    if (dropInViewController == nil) {
//        return;
//    }
//    
//    // The way you present your BTDropInViewController instance is up to you.
//    // In this example, we wrap it in a new, modally presented navigation controller:
//    dropInViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
//                                                                                                          target:self
//                                                                                                          action:@selector(userDidCancelPayment)];
//    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:dropInViewController];
//    [self presentViewController:navigationController animated:YES completion:nil];
}

 //BrainTree Nonce

- (void)postNonceToServer:(NSString *)paymentMethodNonce amount:(int) amount{
    // Update URL with your server
    
    NSString *urlStr = [NSString stringWithFormat:@"http://playentertainmentnetwork.com/MOBILESTEVENGROUP.COM/server/braintree/pay_sandbox.php&payment_method_nonce=%@&amount=%d", paymentMethodNonce, amount];
    
    // http://www.api.postmobapp.com/PostMob/index.php?action=payment&payment_method_nonce=%@&amount=%d
    
    NSURL *paymentURL = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:paymentURL];
    
    //request.HTTPBody = [[NSString stringWithFormat:@"payment_method_nonce=%@", paymentMethodNonce] dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               // TODO: Handle success and failure
                               
                               if (connectionError) {
                                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                       message:@"Payment Failed"
                                                                                      delegate:self
                                                                             cancelButtonTitle:@"OK"
                                                                             otherButtonTitles:nil, nil] ;
                                   alertView.alertViewStyle = UIAlertViewStyleDefault ;
                                   [alertView show] ;
                               }
                               
                           }];
}


//Braintree

- (void)paymentBrainTree : (int) amount {
    
    BTClientCardRequest *request = [BTClientCardRequest new];
    request.number = @"4111111111111111";
    request.expirationMonth = @"11";
    request.expirationYear = @"2023"; //[PMAppDelegate sharedDelegate].globalUser_ExpYear;
    //request.cvv = [PMAppDelegate sharedDelegate].globalUser_CCVCode;
    
    [self.braintree tokenizeCard:request
                      completion:^(NSString* nonce, NSError* error){
                          // Communicate the nonce to your server, or handle error
                          if (!error) {
                              [self postNonceToServer:nonce amount:amount];
                          }
                      }];
}







- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewController:(__unused BTDropInViewController *)viewController didSucceedWithPaymentMethod:(BTPaymentMethod *)paymentMethod {
    [self postNonceToServer:paymentMethod.nonce]; // Send payment method nonce to your server
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce {
    // Update URL with your server
    NSURL *paymentURL = [NSURL URLWithString:@"http://playentertainmentnetwork.com/MOBILESTEVENGROUP.COM/server/braintree/payment.php"]; //
    //playentertainmentnetwork.com/MOBILESTEVENGROUP.COM/server/braintree/pay.php
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:paymentURL];
    request.HTTPBody = [[NSString stringWithFormat:@"payment_method_nonce=%@", paymentMethodNonce] dataUsingEncoding:NSUTF8StringEncoding];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               // TODO: Handle success and failure
                           }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
